package hello;

public class Config {

    private final String driverClassName;
    private final String password;

    public Config(String driverClassName, String password) {
        this.driverClassName = driverClassName;
        this.password = password;
    }

    public String getDriverClassName() {
        return this.driverClassName;
    }

    public String getPassword() {
        return this.password;
    }    
}
